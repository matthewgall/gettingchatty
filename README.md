# matthewgall/gettingchatty

Made as part of [Technocamps](http://technocamps.com), an EU Convergence European Social Funded project through the Welsh Government, was designed to demonstrate web development through interactive website applications. matthewgall/gettingchatty has a JSON powered API to allow users to send basic calls, through Python/PHP and nodejs applications and is fully platform agnostic.

## Requirements
matthewgall/gettingchatty requires the following:

* PHP 5.4.x with the MongoDB extension enabled. Instructions to add the MongoDB extension can be found by [clicking here](http://docs.mongodb.org/ecosystem/drivers/php)
* bower - for the installation of jQuery, jQuery timeago and Bootstrap. For information on installation, [click here](http://bower.io)
* mongodb - for storage of data submitted to the application, you can use a service such as [MongoHQ](http://mongohq.com) or [MongoLab](http://mongolab.com) which both offer free storage.

## Installation

    git clone git@bitbucket.org:matthewgall/gettingchatty.git
    cd gettingchatty
    bower install
    php -S localhost:8000
    
## Licence
All the scripts included in this repository are licenced under the MIT licence.

    Copyright (c) 2013 Matthew Gall

    Permission is hereby granted, free of charge, to any person obtaining a copy of
    this software and associated documentation files (the "Software"), to deal in
    the Software without restriction, including without limitation the rights to
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
    the Software, and to permit persons to whom the Software is furnished to do so,
    subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
    FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
    COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
    IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
    CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Support
Need help? I'd be more than happy to! You can use any of the following methods:

* Create an issue over on the [issue tracker](https://bitbucket.org/matthewgall/gettingchatty/issues) (no registration required) with all the necessary steps to replicate the issue, and the environment you are using;