<?php

require '../vendor/autoload.php';

// Establish a connection to MongoDB
$mongo = \MongoQB\Builder(array(
    'dsn'   =>	$_ENV["MONGODB_URL"]
);

// And now we query for the latest message!
$content = json_encode( $mongo->orderBy(array("_id" => -1))->limit(1)->get('messages') );

// And return them, no modification required
echo $content;

?>
