<?php

require '../vendor/autoload.php';

// First, get the information I need!
$_name = filter_var( $_GET["name"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW );
$_message = filter_var( $_GET["message"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW );
$_time = date( "c", time() );

// Declare a new array to store results of execution
$return = array();
$return["status"] = "success";

$mongo = \MongoQB\Builder(array(
    'dsn'   =>	$_ENV["MONGODB_URL"]
);

// Right, time for a little debugging!
if ( $_name == "" or $_name == null ) { $return['status'] = "error"; $return['message'] = "You did not send a name with your request"; }
if ( $_message == "" or $_message == null ) { $return['status'] = "error"; $return['message'] = "You did not send a message with your request"; }

// Now to make sure I don't do more execution than necessary!
switch ( $return['status'] ) {

	case "error":
		// They have already caused an error, json_encode() the array and return!
		echo json_encode( $return );
		break;

	default:
		// We have all the data we need, now to process it!
		$content = $mongo->orderBy(array("_id" => -1))->limit(1)->get('messages');

		// Now for a little checking!
		if ( $content['name'] == $_name and $content['message'] == $_message ) {
			$return['status'] = "error";
			$return['message'] = "Your message has already been queued";
		}
		else {

			// Log the successful message to MongoDB
			$insert = $mongo->insert('messages', [
    			'name'  	=>  $_name,
    			'message'   =>  $_message,
    			'time'	 	=>  $_time,
    			'ipaddr'	=>	$_SERVER["REMOTE_ADDR"]
			]);
			
			// Now to return success!
            if ( $insert )
            {			
                $return['status'] = "success";
			    $return['message'] = "Your message has been queued";
            }

		}

        echo json_encode( $return );
        exit();
}
