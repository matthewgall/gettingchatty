/**
    Copyright (c) 2013 Matthew Gall

    Permission is hereby granted, free of charge, to any person obtaining a copy of
    this software and associated documentation files (the "Software"), to deal in
    the Software without restriction, including without limitation the rights to
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
    the Software, and to permit persons to whom the Software is furnished to do so,
    subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
    FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
    COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
    IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
    CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

// Some variable declarations!
var lastMessage;
var count = 1;
var direction = "left"

// Fix a bug in IE
$.ajaxSetup({ cache: false });

function getLastMessage() {

	$.getJSON("/api/getMessage.php", function(json) {
		// If we get here, the JSON has been fetched!
		if ( lastMessage != json.message ) {

			// Check if we have reached our maximum messages!
			if ( count > 3 ) {
				// We need to reset!
				$('#messagesPane').empty();
				// And reset direction and counter
				count = 1;
				direction = "left";

			}

			// We need to update! So let's process that!
			$('#messagesPane').append("<div class=\"messageIndividual\" id=\"" + direction + "\"><p>"+ json.message + "</p><small class=\"timeago\" title=\"" + json.time + "\"></small></div>");
			
			// Then we need to re-invoke timeago()
			$("small.timeago").timeago();

			// Then we decide the next direction!
			if ( direction == "left" ) {
				direction = "right";
			}
			else {
				direction = "left";
			}

			// And then we keep a record of the last message we displayed
			lastMessage = json.message;
			// And increment the count by 1
			count++;
		}
	});

}

$(document).ready( function() {

	// Clear any default text in #messagesPane
	$('#messagesPane').empty();

	// So run it once, to get the current message on the server!
	getLastMessage();

	// Then we tell it to fetch the message every second!
	setInterval(getLastMessage, 1000);
});
